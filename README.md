# TypeScript + OpenAPI (TSOA) Project Template

-   [TypeScript + OpenAPI (TSOA) Project Template](#typescript--openapi-tsoa-project-template)
    -   [Clone a new project from this template](#clone-a-new-project-from-this-template)
        -   [Clone this repo](#clone-this-repo)
        -   [Initialize project repository](#initialize-project-repository)

## Clone a new project from this template

### Clone this repo

```
git clone https://gitlab.com/jmjohnson85/tsoa-api-template.git [project-name]
```

### Initialize project repository

```
.../[project-name]$ npm run init

📦 Enter new package name: (tsoa-api-template) [new-package-name]
📘 Enter new package author: (Jeremy Johnson) [new-package-author]
🔖 Enter new package description: [new-package-description]
🌐 Enter new repo remote URL (WARNING: any repo at this URL will be wiped out!): [new-remote-url]
...
Finished! 🚀
```
