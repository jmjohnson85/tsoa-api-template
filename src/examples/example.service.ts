import { BaseService } from '../_base/base.service.js'
import { Example } from './example.model.js'

export class ExampleService extends BaseService {
    public async get(id: number): Promise<Example> {
        const example: Example = { id, name: 'This is just an example...' }
        return example
    }

    public async getAll(): Promise<Example[]> {
        const examples: Example[] = [
            { id: 1, name: 'This was just example...' },
            { id: 2, name: 'This was just another example...' },
        ]
        return examples
    }
}

export default new ExampleService()
