import { Get, Route, Tags, Response, Security, Middlewares as _, Path } from 'tsoa'
import { ValidateErrorBody } from '../utilities/error.types.js'
import { Example } from './example.model.js'
import ExampleService from './example.service.js'
import BaseController from '../_base/base.controller.js'

@Route('v1/examples')
@Tags('Examples')
// @Middlewares()
export class ExampleController extends BaseController {
    @Get('{id}')
    @Response(403, 'Access Denied')
    @Response<ValidateErrorBody>(422, 'Unprocessable Entity')
    @Security('oauth2_clientCredentials')
    public async getById(@Path() id: number): Promise<Example> {
        return await ExampleService.get(id)
    }

    @Get()
    @Response(403, 'Access Denied')
    @Response<ValidateErrorBody>(422, 'Unprocessable Entity')
    @Security('oauth2_clientCredentials')
    public async getAll(): Promise<Example[]> {
        return await ExampleService.getAll()
    }
}

export default new ExampleController()
