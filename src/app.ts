import express from 'express'
import swaggerUi from 'swagger-ui-express'
import swaggerDoc from '../build/swagger.json' assert { type: 'json' }
import { RegisterRoutes } from '../build/routes.js'
import logger from './utilities/logger.js'
import { loggerHandler } from './utilities/logger.handler.js'
import ErrorHandler from './utilities/error.handler.js'

const app = express()

main()

async function main() {
    app.use(loggerHandler)

    app.use(express.urlencoded({ extended: true }))
    app.use(express.json())

    const swaggerUiHandler = swaggerUi.setup(swaggerDoc)
    app.use('/v1/docs', swaggerUi.serve, swaggerUiHandler)

    // Start any long-lived connections, monitors, etc here

    RegisterRoutes(app)

    const error = new ErrorHandler(logger)
    app.use(error.handleMissedRoutes())
    app.use(error.handleValidationErrors())
}

export default app
