import pino, { Logger } from 'pino'

const logger: Logger = pino({
  name: process.env.APP_ID,
  level: process.env.LOG_LEVEL,
})

export default logger