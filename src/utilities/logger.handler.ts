import { Request, Response, NextFunction } from 'express'
import logger from './logger.js'

export function loggerHandler(req: Request, _res: Response, next: NextFunction): void {
    logger.info(req, '[Express] received request')
    next()
}
