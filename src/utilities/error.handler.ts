import { HandlerResponse, ValidateErrorBody } from './error.types.js'
import { Request, Response, NextFunction } from 'express'
import { ValidateError } from 'tsoa'
import { Logger } from 'pino'

export default class ErrorHandler {
    private logger: Logger

    public constructor(logger: Logger) {
        this.logger = logger
    }

    public handleMissedRoutes() {
        return this.notFound.bind(this)
    }

    public handleValidationErrors() {
        return this.notValid.bind(this)
    }

    private notFound(_req: Request, res: Response): void {
        return this[404](res)
    }

    private notValid(err: unknown, req: Request, res: Response, next: NextFunction): HandlerResponse {
        if (err instanceof ValidateError) {
            return this[422](err, req, res)
        } else if (err instanceof Error) {
            return this[500](err, req, res)
        }

        next()
    }

    // HTTP response status codes and messages - https://developer.mozilla.org/en-US/docs/Web/HTTP/Status

    private 404(res: Response): void {
        const message = 'Not Found'
        res.status(404).send({ message })
    }

    private 422(err: ValidateError, req: Request, res: Response) {
        const message = 'Unprocessable Entity'
        this.logger.warn(`${message}: ${req.path}`, err?.fields)
        const body: ValidateErrorBody = { message, details: err?.fields }
        return res.status(422).json(body)
    }

    private 500(err: Error, req: Request, res: Response) {
        const message = 'Internal Server Error'
        this.logger.error(`${message}: ${req.path}`)
        this.logger.error(err)
        const body = { message }
        return res.status(500).json(body)
    }
}
