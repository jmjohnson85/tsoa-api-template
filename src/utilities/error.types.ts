import { Response } from 'express'
import { FieldErrors } from 'tsoa'

export type HandlerResponse = (
    Response | void
)

export type ValidateErrorBody = {
    message: string,
    details: FieldErrors
}