import './utilities/env.js'
import logger from './utilities/logger.js'
import app from './app.js'
import http from 'http'

const port = process.env.PORT || 3000

const listener = function () {
    const env = process.env.NODE_ENV || 'dev'
    const message = `[Express] Running in ${env} on port: ${port}`
    logger.info(message)
}

const server = http.createServer(app)
server.listen(port, listener)
