FROM node:16-alpine

RUN mkdir -p /app
WORKDIR /app

COPY . /app

RUN npm ci --no-fund --no-audit --no-update-notifier --progress=false --ignore-scripts --omit=dev

EXPOSE 3000

CMD [ "npm", "start" ]
