# TypeScript + OpenAPI (TSOA) Example Project

-   [TypeScript + OpenAPI (TSOA) Example Project](#typescript--openapi-tsoa-example-project)
    -   [Documentation](#documentation)
    -   [Initialize Project](#initialize-project)
    -   [Start API in Development](#start-api-in-development)
        -   [View Swagger / OpenAPI Docs](#view-swagger--openapi-docs)
    -   [Start API in Production](#start-api-in-production)

## Documentation

-   [tsoa: OpenAPI-compliant Web APIs using TypeScript and Node](https://tsoa-community.github.io/docs/introduction.html)

## Initialize Project

```sh
$ npm run init
```

## Start API in Development

```sh
$ npm run dev
```

### View Swagger / OpenAPI Docs

-   Navigate to http://localhost:3000/v1/docs

## Start API in Production

```sh
$ npm start
```
